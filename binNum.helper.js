function binaryNum(num){
	if(!(this instanceof binaryNum)){
		return new binaryNum(num);
	}

	if(num !== undefined){
		return this.use(num);
	}
}

binaryNum.prototype.setBit = function(num, bitNumber){
	return num | (1 << bitNumber);
};

binaryNum.prototype.clearBit = function(num, bitNumber){
	return num & ~(1 << bitNumber);
};

binaryNum.prototype.toggleBit = function(num, bitNumber){
	return num ^ (1 << bitNumber);
};

binaryNum.prototype.readBit = function(num, bitNumber){
	return (num & (1 << bitNumber)) >> bitNumber;
};

binaryNum.prototype.use = function(num){
	if(Number.isInteger(num)){
		this.__used = {
				num,
				bitNumber: undefined,
			};

		return this;
	} else {
		this.__used = undefined;
		throw 'Error: number must be integer';
	}
};

binaryNum.prototype.bit = function(bitNumber){
	if(Number.isInteger(bitNumber)){
		this.__used.bitNumber = bitNumber;

		return this;
	} else {
		this.__used = undefined;
		throw 'Error: bitNumber is not integer';
	}
};

binaryNum.prototype.get = function(){
	if(this.__used.num == null || this.__used.bitNumber == null){
		throw 'Error: should call use() and bit() before calling get()';
	}

	const value = this.readBit(this.__used.num, this.__used.bitNumber);
	this.__used = undefined;

	return value;
};

binaryNum.prototype.set = function(bitValue){
	if(this.__used.num == null || this.__used.bitNumber == null){
		throw 'Error: should call use() and bit() before calling set()';
	}

	if(bitValue === undefined){
		bitValue = 1;
	}

	if(Number.isInteger(bitValue)){
		const value = +bitValue & 1
			? this.setBit(this.__used.num, this.__used.bitNumber)
			: this.clearBit(this.__used.num, this.__used.bitNumber);

		this.__used = undefined;

		return value;
	} else {
		throw 'Error: bitValue is not integer';
	}
};

binaryNum.prototype.toggle = function(){
	if(this.__used.num == null || this.__used.bitNumber == null){
		throw 'Error: should call use() and bit() before calling toggle()';
	}

	const value = this.toggleBit(this.__used.num, this.__used.bitNumber);
	this.__used = undefined;

	return value;
};

binaryNum.prototype.read = function(){
	return this.get();
};

binaryNum.prototype.write = function(bitValue){
	return this.set(bitValue);
};

binaryNum.prototype.clear = function(){
	return this.set(0);
};

module.exports = binaryNum;
